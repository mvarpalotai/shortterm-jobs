
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.GroupDAO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    
    private GroupService underTest;
    
    @Mock
    GroupDAO groupDao;
    
    public GroupServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        underTest = new GroupService(groupDao);
    }
    
    @After
    public void tearDown() {
    }

   
    @Test
    public void testFindUserGroupThenInvokegroupDaoOneTime() throws Exception {
        underTest.findUserGroup();
        Mockito.verify(groupDao, Mockito.times(1)).findGroupByName(ArgumentMatchers.anyString());
    }

 
    @Test
    public void testFindAdminGroupThenInvokeGroupDaoOneTime() throws Exception {
        underTest.findAdminGroup();
        Mockito.verify(groupDao, Mockito.times(1)).findGroupByName(ArgumentMatchers.anyString());
    }
    
}
