
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.JobApplicantDataDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ImageUploadServiceTest {
    
    private ImageUploadService underTest;
    
    @Mock
    private JobApplicantDataDAO jobApplicantDataDao;
    
    @Mock 
    private StjUserDAO stjUserDAO;
    
    
    public ImageUploadServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        underTest = new ImageUploadService(jobApplicantDataDao, stjUserDAO);
    }
    
    @After
    public void tearDown() {
    }


    @Test
    public void testPersistProfileImagePathThenDaosInvokeOneTime() throws Exception {
        StjUserEntity userEntity = new StjUserEntity();
        JobApplicantDataEntity dataEntity = new JobApplicantDataEntity();
        Mockito.when(stjUserDAO.getUserByEmail(ArgumentMatchers.anyString())).thenReturn(userEntity);
        Mockito.when(jobApplicantDataDao.findDataByUser(ArgumentMatchers.any())).thenReturn(dataEntity);
        

        underTest.persistProfileImagePath("email", "path");
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail(ArgumentMatchers.anyString());
        Mockito.verify(jobApplicantDataDao, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
        Mockito.verify(jobApplicantDataDao, Mockito.times(1)).updatePersonalDatas(ArgumentMatchers.any());
    }
    
}
