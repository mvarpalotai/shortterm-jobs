package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.UserTypeDAO;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserTypeServiceTest {

    private UserTypeService underTest;

    @Mock
    UserTypeDAO userTypeDAO;

    public UserTypeServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        underTest = new UserTypeService(userTypeDAO);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFindUserTypeByNameThenDaoInvokeOneTime() throws Exception {
        underTest.findUserTypeByName(ArgumentMatchers.anyString());
        Mockito.verify(userTypeDAO, Mockito.times(1)).findUserTypeByName(ArgumentMatchers.anyString());
    }

    @Test
    public void testListUserTypeThenDaoInvokeOneTime() throws Exception {
        underTest.listUserType();
        Mockito.verify(userTypeDAO, Mockito.times(1)).listUserType();
    }

}
