package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.EmployerDataDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.entity.EmployerDataEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import java.security.Principal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EmployerProfileServiceTest {

    private EmployerProfileService underTest;

    @Mock
    EmployerDataDAO employerDataDAO;

    @Mock
    StjUserDAO stjUserDAO;

    @Mock
    ProfileService profileService;

    public EmployerProfileServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        underTest = new EmployerProfileService(employerDataDAO, stjUserDAO, profileService);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFindEmployerDataByUserIdThenDAOsRunOneTime() {
        StjUserEntity userEntity = new StjUserEntity();
        EmployerDataEntity dataEntity = new EmployerDataEntity();
        dataEntity.setUser(userEntity);

        Mockito.when(stjUserDAO.getStjUserById(ArgumentMatchers.anyInt())).thenReturn(userEntity);
        Mockito.when(employerDataDAO.findDataByUser(ArgumentMatchers.any())).thenReturn(dataEntity);
        underTest.findEmployerDataByUserId(ArgumentMatchers.anyInt());
        Mockito.verify(stjUserDAO, Mockito.times(1)).getStjUserById(ArgumentMatchers.anyInt());
        Mockito.verify(employerDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
    }
    
    @Test
    public void testFindEmployerDataByPrincipalThenDAOsRunOneTime() {
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return "principal@principal.hu";
            }
        };
        StjUserEntity userEntity = new StjUserEntity();
        EmployerDataEntity dataEntity = new EmployerDataEntity();
        dataEntity.setUser(userEntity);

        Mockito.when(stjUserDAO.getUserByEmail(ArgumentMatchers.anyString())).thenReturn(userEntity);
        Mockito.when(employerDataDAO.findDataByUser(ArgumentMatchers.any())).thenReturn(dataEntity);
        underTest.findEmployerDataByPrincipal(principal);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail(ArgumentMatchers.anyString());
        Mockito.verify(employerDataDAO, Mockito.times(1)).findDataByUser(ArgumentMatchers.any());
    }
    

    @Test
    public void testFindUserByPrincipalThenProfileServiceRunOneTime() throws Exception {
        underTest.findUserByPrincipal(ArgumentMatchers.any());
        Mockito.verify(profileService, Mockito.times(1)).findUserByPrincipal(ArgumentMatchers.any());
    }

    @Test
    public void testFindUserByIdThenProfileServiceRunOneTime() throws Exception {
        underTest.findUserByUserId(ArgumentMatchers.anyInt());
        Mockito.verify(profileService, Mockito.times(1)).findUserById(ArgumentMatchers.any());
    }
    
    @Test
    public void testUpdateEmployerDataByDataIdThenDAOsInvokeOneTime() throws Exception {
        EmployerDataEntity dataEntity = new EmployerDataEntity();
        Mockito.when(employerDataDAO.findDataByDataId(ArgumentMatchers.anyInt())).thenReturn(dataEntity);
        underTest.updateEmployerDataByDataId(1, "Test Test",
                "test@test.com", "Test Street 1", "150",
                "description", "business scope", "hr contact");
        
        Mockito.verify(employerDataDAO, Mockito.times(1)).findDataByDataId(ArgumentMatchers.anyInt());
        Mockito.verify(employerDataDAO, Mockito.times(1)).updateEmployerData(ArgumentMatchers.any());
    }

}
