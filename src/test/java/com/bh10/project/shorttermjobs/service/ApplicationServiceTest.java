package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.AdvertisementDAO;
import com.bh10.project.shorttermjobs.dao.ApplicationDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.dto.ApplicationDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import java.security.Principal;
import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceTest {

    private ApplicationService underTest;

    @Mock
    AdvertisementDAO advertisementDAO;

    @Mock
    StjUserDAO stjUserDAO;

    @Mock
    ApplicationDAO applicationDAO;

    @Mock
    AdvertisementService advertisementService;

    public ApplicationServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        underTest = new ApplicationService(advertisementDAO, stjUserDAO, applicationDAO, advertisementService);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFindAdvertisementByIdThenInvokeDaoOneTime() throws Exception {
        underTest.findAdvertisementById(ArgumentMatchers.anyInt());
        Mockito.verify(advertisementDAO, Mockito.times(1)).findAdvertisementById(ArgumentMatchers.anyInt());
    }

    @Test
    public void testFindAdvertisementByIdThenInvokeDaoWithGivenId() throws Exception {
        underTest.findAdvertisementById(1);
        Mockito.verify(advertisementDAO, Mockito.times(1)).findAdvertisementById(1);
    }

    @Test
    public void testFindUserByPrincipalThenInvokeDaoWithGivenPrincipal() throws Exception {
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return "email";
            }
        };
        underTest.findUserByPrincipal(principal);
        Mockito.verify(stjUserDAO, Mockito.times(1)).getUserByEmail("email");
    }

    @Test
    public void testCreateApplication() throws Exception {

    }

    @Test
    public void testFindApplicantByAdvertisement() throws Exception {

    }

    @Test
    public void testFindApplicantByAdvertisementId() throws Exception {

    }

    @Test
    public void testGetAdvertisementAppliedByUser() throws Exception {

    }

}
