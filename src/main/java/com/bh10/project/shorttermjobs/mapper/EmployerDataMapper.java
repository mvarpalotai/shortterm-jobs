/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.EmployerDataDto;
import com.bh10.project.shorttermjobs.entity.EmployerDataEntity;

/**
 *
 * @author Maxim
 */
public final class EmployerDataMapper {

        public static EmployerDataDto toDto(EmployerDataEntity entity) {
        EmployerDataDto dto = new EmployerDataDto();
        dto.setId(entity.getId());
        dto.setHrContact(entity.getHrContact());
        dto.setAddress(entity.getAddress());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setSize(entity.getSize());
        dto.setDescription(entity.getDescription());
        dto.setBusinessScope(entity.getBusinessScope());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());
        return dto;
    }
}
