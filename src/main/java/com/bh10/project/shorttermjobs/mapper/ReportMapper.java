/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.ReportDto;
import com.bh10.project.shorttermjobs.entity.ReportEntity;

/**
 *
 * @author csubi
 */
public final class ReportMapper {
    
    public static ReportEntity toEntity(ReportDto dto){
        ReportEntity entity = new ReportEntity();
        
        entity.setAdvertisement(AdvertisementMapper.toEntity(dto.getAdvertisementDto()));
        entity.setDescription(dto.getDescription());
        entity.setId(dto.getId());
        entity.setReporterUser(StjUserMapper.toEntity(dto.getStjUserDto()));
        entity.setStatus(dto.getStatus());
        
        return entity;
    }
    
    public static ReportDto toDto(ReportEntity entity){
        ReportDto dto = new ReportDto();
        
        dto.setAdvertisementDto(AdvertisementMapper.toDto(entity.getAdvertisement()));
        dto.setDescription(entity.getDescription());
        dto.setId(entity.getId());
        dto.setStatus(entity.getStatus());
        dto.setStjUserDto(StjUserMapper.toDto(entity.getReporterUser()));
        
        return dto;
    }
    
}
