/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.ApplicationDto;
import com.bh10.project.shorttermjobs.entity.ApplicationEntity;

/**
 *
 * @author Maxim
 */
public final class ApplicationMapper {
    
     public static ApplicationDto toDto(ApplicationEntity entity) {
        ApplicationDto dto = new ApplicationDto();
        dto.setId(entity.getId());
        dto.setAdvertisement(AdvertisementMapper.toDto(entity.getAdvertisement()));
        dto.setUser(StjUserMapper.toDto(entity.getUser()));
        return dto;
    }
}
