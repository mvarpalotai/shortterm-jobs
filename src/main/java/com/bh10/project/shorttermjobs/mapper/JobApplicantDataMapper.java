/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.JobApplicantDataDto;
import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;

/**
 *
 * @author Maxim
 */
public final class JobApplicantDataMapper {
    
        public static JobApplicantDataEntity toEntity(JobApplicantDataDto dto) {
        JobApplicantDataEntity entity = new JobApplicantDataEntity();
        entity.setId(dto.getId());
        entity.setAddress(dto.getAddress());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setDescription(dto.getDescription());
        entity.setCvUrl(dto.getCvUrl());
        entity.setCvEmail(dto.getCvEmail());
        entity.setCvName(dto.getCvName());
        entity.setProfImgPath(dto.getProfImgPath());
        return entity;
    }
        
        public static JobApplicantDataDto toDto(JobApplicantDataEntity entity) {
        JobApplicantDataDto dto = new JobApplicantDataDto();
        dto.setId(entity.getId());
        dto.setAddress(entity.getAddress());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setDescription(entity.getDescription());
        dto.setCvUrl(entity.getCvUrl());
        dto.setCvEmail(entity.getCvEmail());
        dto.setCvName(entity.getCvName());
        dto.setProfImgPath(entity.getProfImgPath());
        return dto;
    }
}
