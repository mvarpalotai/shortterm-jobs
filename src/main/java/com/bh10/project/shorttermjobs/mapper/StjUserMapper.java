package com.bh10.project.shorttermjobs.mapper;

import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;

public final class StjUserMapper {

    public static StjUserEntity toEntity(StjUserDto dto) {
        StjUserEntity entity = new StjUserEntity();
        entity.setId(dto.getId());
        entity.setEmail(dto.getEmail());
        entity.setName(dto.getName());
        entity.setPassword(dto.getPassword());
        return entity;
    }

    public static StjUserDto toDto(StjUserEntity entity) {
        StjUserDto dto = new StjUserDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setName(entity.getName());
        dto.setPassword(entity.getPassword());
        return dto;
    }

}
