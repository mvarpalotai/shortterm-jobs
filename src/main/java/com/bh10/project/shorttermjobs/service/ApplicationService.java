
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.AdvertisementDAO;
import com.bh10.project.shorttermjobs.dao.ApplicationDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.AdvertisementDto;
import com.bh10.project.shorttermjobs.dto.ApplicationDto;
import com.bh10.project.shorttermjobs.entity.AdvertisementEntity;
import com.bh10.project.shorttermjobs.entity.ApplicationEntity;
import com.bh10.project.shorttermjobs.entity.StjUserEntity;
import com.bh10.project.shorttermjobs.exception.UserAlreadyAppliedException;
import com.bh10.project.shorttermjobs.exception.UserHasNoApplicationException;
import com.bh10.project.shorttermjobs.mapper.ApplicationMapper;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;


@Singleton
public class ApplicationService {

    @Inject
    AdvertisementDAO advertisementDAO;
    
    @Inject
    StjUserDAO stjUserDAO;
    
    @Inject
    ApplicationDAO applicationDAO;
    
    @Inject
    AdvertisementService advertisementService;

    public ApplicationService() {
    }

    public ApplicationService(AdvertisementDAO advertisementDAO, StjUserDAO stjUserDAO, ApplicationDAO applicationDAO, AdvertisementService advertisementService) {
        this.advertisementDAO = advertisementDAO;
        this.stjUserDAO = stjUserDAO;
        this.applicationDAO = applicationDAO;
        this.advertisementService = advertisementService;
    }
    
    
    
    public AdvertisementEntity findAdvertisementById(Integer id) {
       return advertisementDAO.findAdvertisementById(id);
    }
    
    public StjUserEntity findUserByPrincipal(Principal principal) {
       return stjUserDAO.getUserByEmail(principal.getName());
    }
    
    public void createApplication(Integer id, Principal principal) throws UserAlreadyAppliedException {

        List<ApplicationEntity> applicationListByUserId = null;
        applicationListByUserId = applicationDAO.applicationListByUserId(findUserByPrincipal(principal).getId());

        if (null != applicationListByUserId) {
            for (ApplicationEntity applicationEntity : applicationListByUserId) {
                if (applicationEntity.getAdvertisement().getId().equals(id)) {
                    throw new UserAlreadyAppliedException();
                }
            }
        }
        
        ApplicationEntity entity = new ApplicationEntity();
        entity.setAdvertisement(findAdvertisementById(id));
        entity.setUser(findUserByPrincipal(principal));
        applicationDAO.createApplication(entity);

    }
    
    public List<ApplicationDto> findApplicantByAdvertisement(AdvertisementEntity entity) {
       List<ApplicationEntity> list = applicationDAO.getApplicantForAdvertisement(entity);
       List<ApplicationDto> dtos = new ArrayList<>();
       list.forEach((p) -> {
           dtos.add(ApplicationMapper.toDto(p));
       });
       return dtos;
    }
    public List<ApplicationDto> findApplicantByAdvertisementId(Integer advertisementId) {
       List<ApplicationEntity> list = applicationDAO.getApplicantForAdvertisementId(advertisementId);
       List<ApplicationDto> dtos = new ArrayList<>();
       list.forEach((p) -> {
           dtos.add(ApplicationMapper.toDto(p));
       });
       return dtos;
    }
    
        
    public List<AdvertisementDto> getAdvertisementAppliedByUser(Integer userId) throws UserHasNoApplicationException{
        List<AdvertisementDto> allAdvertisementList = new ArrayList<>();
        List<AdvertisementDto> advertisementsAppliedByUserList = new ArrayList<>();
        List<ApplicationEntity> applicationsByUser = new ArrayList<>();
        
        allAdvertisementList = advertisementService.findAllAdvertisement();
        applicationsByUser = applicationDAO.applicationListByUserId(userId);
        
       if (applicationsByUser.size() < 1){
           throw new UserHasNoApplicationException();
        }
        
            for (AdvertisementDto advertisementDto : allAdvertisementList) {
                for (ApplicationEntity applicationEntity : applicationsByUser) {
                    if (advertisementDto.getId().equals(applicationEntity.getAdvertisement().getId())){
                        advertisementsAppliedByUserList.add(advertisementDto);
                    }
                }
            }
        
        return advertisementsAppliedByUserList;
        
    }
    
}
