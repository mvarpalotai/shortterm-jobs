
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.GroupDAO;
import com.bh10.project.shorttermjobs.entity.GroupEntity;
import com.bh10.project.shorttermjobs.exception.EmailIsUsedException;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author csubi
 */
@Singleton
public class GroupService {
    
    @Inject
    GroupDAO groupDAO;

    public GroupService() {
    }

    public GroupService(GroupDAO groupDAO) {
        this.groupDAO = groupDAO;
    }
    

    public GroupEntity findUserGroup(){
        return groupDAO.findGroupByName("user");
    }
    
    public GroupEntity findAdminGroup(){
        return groupDAO.findGroupByName("admin");
    }
}
