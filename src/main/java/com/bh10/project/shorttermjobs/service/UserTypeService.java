/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.UserTypeDAO;
import com.bh10.project.shorttermjobs.entity.UserTypeEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author csubi
 */
@Singleton
public class UserTypeService {
    
    @Inject
    UserTypeDAO userTypeDAO;

    public UserTypeService() {
    }

    public UserTypeService(UserTypeDAO userTypeDAO) {
        this.userTypeDAO = userTypeDAO;
    }
    
    
    
    public UserTypeEntity findUserTypeByName(String name){
        return userTypeDAO.findUserTypeByName(name);
    }
    
    public List<UserTypeEntity> listUserType(){
        return userTypeDAO.listUserType();
    }
    
}
