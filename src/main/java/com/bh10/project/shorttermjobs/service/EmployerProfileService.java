/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.service;

import com.bh10.project.shorttermjobs.dao.EmployerDataDAO;
import com.bh10.project.shorttermjobs.dao.StjUserDAO;
import com.bh10.project.shorttermjobs.dto.EmployerDataDto;
import com.bh10.project.shorttermjobs.dto.StjUserDto;
import com.bh10.project.shorttermjobs.entity.EmployerDataEntity;
import com.bh10.project.shorttermjobs.mapper.EmployerDataMapper;
import com.bh10.project.shorttermjobs.mapper.StjUserMapper;
import java.security.Principal;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Maxim
 */
@Singleton
public class EmployerProfileService {

    @Inject
    EmployerDataDAO employerDataDAO;

    @Inject
    StjUserDAO stjUserDAO;

    @Inject
    ProfileService profileService;

    public EmployerProfileService() {
    }

    public EmployerProfileService(EmployerDataDAO employerDataDAO, StjUserDAO stjUserDAO, ProfileService profileService) {
        this.employerDataDAO = employerDataDAO;
        this.stjUserDAO = stjUserDAO;
        this.profileService = profileService;
    }
    
    

    public EmployerDataDto findEmployerDataByPrincipal(Principal principal) {
        return EmployerDataMapper.toDto(employerDataDAO.findDataByUser(stjUserDAO.getUserByEmail(principal.getName())));
    }

    public EmployerDataDto findEmployerDataByUserId(Integer userId) {
        return EmployerDataMapper.toDto(employerDataDAO.findDataByUser(stjUserDAO.getStjUserById(userId)));
    }

    public StjUserDto findUserByPrincipal(Principal principal) {
        return profileService.findUserByPrincipal(principal);
    }

    public StjUserDto findUserByUserId(Integer id) {
        return profileService.findUserById(id);
    }

    public void updateEmployerDataByDataId(Integer id, String name, String email, String address, String size, String description, String businessScope, String hrContact) {
        EmployerDataEntity entity = employerDataDAO.findDataByDataId(id);
        entity.setName(name);
        entity.setEmail(email);
        entity.setAddress(address);
        entity.setSize(size);
        entity.setDescription(description);
        entity.setBusinessScope(businessScope);
        entity.setHrContact(hrContact);
        employerDataDAO.updateEmployerData(entity);
    }
}
