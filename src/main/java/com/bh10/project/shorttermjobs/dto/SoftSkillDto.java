package com.bh10.project.shorttermjobs.dto;

public class SoftSkillDto {

    private Integer id;
    private String name;
    private Integer level;

    public SoftSkillDto() {
    }

    public SoftSkillDto(Integer id, String name, Integer level) {
        this.id = id;
        this.name = name;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "SoftSkillDto{" + "id=" + id + ", name=" + name + ", level=" + level + '}';
    }

}
