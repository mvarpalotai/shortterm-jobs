package com.bh10.project.shorttermjobs.dto;

public class ReportDto {

    private Integer id;
    private String description;
    private String status;
    private StjUserDto stjUserDto;
    private AdvertisementDto advertisementDto;

    public StjUserDto getStjUserDto() {
        return stjUserDto;
    }

    public void setStjUserDto(StjUserDto stjUserDto) {
        this.stjUserDto = stjUserDto;
    }

    public AdvertisementDto getAdvertisementDto() {
        return advertisementDto;
    }

    public void setAdvertisementDto(AdvertisementDto advertisementDto) {
        this.advertisementDto = advertisementDto;
    }

    public ReportDto() {
    }

    public ReportDto(Integer id, String description, String status) {
        this.id = id;
        this.description = description;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ReportDto{" + "id=" + id + ", description=" + description + ", status=" + status + '}';
    }

}
