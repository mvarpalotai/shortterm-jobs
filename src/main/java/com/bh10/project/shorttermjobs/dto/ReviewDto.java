package com.bh10.project.shorttermjobs.dto;

public class ReviewDto {

    private Integer id;
    private String description;
    private Integer score;

    public ReviewDto() {
    }

    public ReviewDto(Integer id, String description, Integer score) {
        this.id = id;
        this.description = description;
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "ReviewDto{" + "id=" + id + ", description=" + description + ", score=" + score + '}';
    }

}
