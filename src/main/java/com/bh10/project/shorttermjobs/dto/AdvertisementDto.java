package com.bh10.project.shorttermjobs.dto;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

public class AdvertisementDto {

    private Integer id;
    private Date date;
    private Integer wage;
    private String location;
    private String jobType;
    private String description;
    private StjUserDto stjUserDto;
    private ReportDto reportDto;
    private String category;
    private List<ApplicationDto> applications;

    public AdvertisementDto() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    
    
    public List<ApplicationDto> getApplications() {
        return applications;
    }

    public void setApplications(List<ApplicationDto> applications) {
        this.applications = applications;
    }

    public ReportDto getReportDto() {
        return reportDto;
    }

    public void setReportDto(ReportDto reportDto) {
        this.reportDto = reportDto;
    }

    public StjUserDto getStjUserDto() {
        return stjUserDto;
    }

    public void setStjUserDto(StjUserDto stjUserDto) {
        this.stjUserDto = stjUserDto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWage() {
        return wage;
    }

    public void setWage(Integer wage) {
        this.wage = wage;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AdvertisementDto other = (AdvertisementDto) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AdvertisementDto{" + "id=" + id + ", date=" + date + ", location=" + location + ", jobType=" + jobType + ", description=" + description + '}';
    }

}
