/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.dao;

import com.bh10.project.shorttermjobs.entity.JobApplicantDataEntity;
import com.bh10.project.shorttermjobs.entity.SoftSkillEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Maxim
 */
@Stateless
public class SoftSkillDAO {

    @PersistenceContext
    EntityManager em;

    public List<SoftSkillEntity> findSoftSkillByJobApplicant(JobApplicantDataEntity jobApplicantDataEntity) {
        return (List<SoftSkillEntity>) em.createQuery("select s from SoftSkillEntity s where s.jobApplicantData = :id")
                .setParameter("id", jobApplicantDataEntity)
                .getResultList();
    }
    
    public SoftSkillEntity findSoftSkillById(Integer id) {
        return (SoftSkillEntity) em.createQuery("select s from SoftSkillEntity s where s.id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
    
     public void updateSoftSkill(SoftSkillEntity entity) {
        em.merge(entity);
    }
     
     public void addNewSoftSkill(SoftSkillEntity entity) {
        em.persist(entity);
    }
     
     public void deleteSoftSkill(SoftSkillEntity entity) {
         em.remove(entity);
     }
        
}
