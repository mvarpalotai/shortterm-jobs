/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bh10.project.shorttermjobs.exception;

/**
 *
 * @author csubi
 */
public class UserAlreadyAppliedException extends Exception{

    public String getMessage(String userEmail, String advertisementID) {
        return "User: " + userEmail + " is already applied for advertisement: " + advertisementID;
    }
    
}
