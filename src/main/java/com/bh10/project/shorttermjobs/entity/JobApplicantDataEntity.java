package com.bh10.project.shorttermjobs.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "job_applicant_data")
public class JobApplicantDataEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    @Column(name = "email")
    private String cvEmail;
    @Column(name = "name")
    private String cvName;
    
    @OneToOne
    @JoinColumn(name = "user_id")
    private StjUserEntity userId;

    private String address;

    @Column(name = "phone_number")
    private String phoneNumber;

    private String description;

    @Column(name = "cv_url")
    private String cvUrl;
    
    @Column(name = "profile_image_path")
    private String profImgPath;

    @OneToMany(mappedBy = "jobApplicantData")
    private List<EducationEntity> educations;

    @OneToMany(mappedBy = "jobApplicantData")
    private List<LanguageEntity> languages;

    @OneToMany(mappedBy = "jobApplicantData")
    private List<HardSkillEntity> hardSkills;

    @OneToMany(mappedBy = "jobApplicantData")
    private List<SoftSkillEntity> softSkills;

    @OneToMany(mappedBy = "jobApplicantData")
    private List<ExperienceEntity> experiences;
    
    

    public JobApplicantDataEntity() {
    }

    public String getProfImgPath() {
        return profImgPath;
    }

    public void setProfImgPath(String profImgPath) {
        this.profImgPath = profImgPath;
    }
    
    

    public List<HardSkillEntity> getHardSkills() {
        return hardSkills;
    }

    public void setHardSkills(List<HardSkillEntity> hardSkills) {
        this.hardSkills = hardSkills;
    }

    public List<SoftSkillEntity> getSoftSkills() {
        return softSkills;
    }

    public void setSoftSkills(List<SoftSkillEntity> softSkills) {
        this.softSkills = softSkills;
    }

    public List<ExperienceEntity> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<ExperienceEntity> experiences) {
        this.experiences = experiences;
    }

    public List<EducationEntity> getEducations() {
        return educations;
    }

    public void setEducations(List<EducationEntity> educations) {
        this.educations = educations;
    }

    public List<LanguageEntity> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageEntity> languages) {
        this.languages = languages;
    }

    public Integer getId() {
        return id;
    }
   
    public void setId(Integer id) {
        this.id = id;
    }

    public StjUserEntity getUserId() {
        return userId;
    }

    public void setUserId(StjUserEntity userId) {
        this.userId = userId;
    }

    

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCvUrl() {
        return cvUrl;
    }

    public void setCvUrl(String cvUrl) {
        this.cvUrl = cvUrl;
    }

    public String getCvEmail() {
        return cvEmail;
    }

    public void setCvEmail(String cvEmail) {
        this.cvEmail = cvEmail;
    }

    public String getCvName() {
        return cvName;
    }

    public void setCvName(String cvName) {
        this.cvName = cvName;
    }
    
    

}
