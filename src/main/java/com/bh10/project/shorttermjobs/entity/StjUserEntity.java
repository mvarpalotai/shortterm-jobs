package com.bh10.project.shorttermjobs.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "stj_user")
@Inheritance(strategy = InheritanceType.JOINED)
public class StjUserEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;
    
    @Column(unique = true)
    private String email;

    private String name;

    private String password;

    @ManyToOne
    @JoinColumn(name = "user_type_id")
    private UserTypeEntity userType;
    
    @OneToOne(mappedBy = "userId")
    JobApplicantDataEntity jobApplicantDataEntity;

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "id")
    private GroupEntity group;

    @OneToOne(mappedBy = "user")
    private EmployerDataEntity employerData;

    @OneToMany(mappedBy = "reporterUser")
    private List<ReportEntity> reports;

    @OneToMany(mappedBy = "user")
    private List<AdvertisementEntity> advertisements;

    @OneToMany(mappedBy = "reviewerUser")
    private List<ReviewEntity> reviewsAsReviewerUser;

    @OneToMany(mappedBy = "reviewedUser")
    private List<ReviewEntity> reviewsAsReviewedUser;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "stj_user_group",
            joinColumns = {
                @JoinColumn(name = "email", referencedColumnName = "email")},
            inverseJoinColumns = {
                @JoinColumn(name = "group_name", referencedColumnName = "name")}
    )
    private List<GroupEntity> groups = new ArrayList<>();

    public StjUserEntity() {
    }

    public EmployerDataEntity getEmployerData() {
        return employerData;
    }

    public void setEmployerData(EmployerDataEntity employerData) {
        this.employerData = employerData;
    }

    public List<GroupEntity> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupEntity> groups) {
        this.groups = groups;
    }

    public List<AdvertisementEntity> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<AdvertisementEntity> advertisements) {
        this.advertisements = advertisements;
    }

    public List<ReviewEntity> getReviewsAsReviewerUser() {
        return reviewsAsReviewerUser;
    }

    public void setReviewsAsReviewerUser(List<ReviewEntity> reviewsAsReviewerUser) {
        this.reviewsAsReviewerUser = reviewsAsReviewerUser;
    }

    public List<ReviewEntity> getReviewsAsReviewedUser() {
        return reviewsAsReviewedUser;
    }

    public void setReviewsAsReviewedUser(List<ReviewEntity> reviewsAsReviewedUser) {
        this.reviewsAsReviewedUser = reviewsAsReviewedUser;
    }

    public List<ReportEntity> getReports() {
        return reports;
    }

    public void setReports(List<ReportEntity> reports) {
        this.reports = reports;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserTypeEntity getUserType() {
        return userType;
    }

    public void setUserType(UserTypeEntity userType) {
        this.userType = userType;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity group) {
        this.group = group;
    }

    public JobApplicantDataEntity getJobApplicantDataEntity() {
        return jobApplicantDataEntity;
    }

    public void setJobApplicantDataEntity(JobApplicantDataEntity jobApplicantDataEntity) {
        this.jobApplicantDataEntity = jobApplicantDataEntity;
    }
    
    

}
