
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Reports</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/reports.css"/>"/>
    </head>
    <body class="report-body">
        <jsp:include page="navbar.jsp"></jsp:include>
        <c:if test="${not empty sessionScope.advertisement}">
            <div class="advert">
                <div class="jobtype">
                    <img class="search-icon" src="<c:url value="resources/images/icons/job_icon.png"/>" alt="Job Icon"><p>${advertisement.jobType}</p>
                </div>
                <div class="location">
                    <img class="search-icon" src="<c:url value="resources/images/icons/location_icon.png"/>" alt="Location Icon"><p>${advertisement.location}</p>
                </div>
                <div class="date">
                    <img class="search-icon" src="<c:url value="resources/images/icons/calendar_icon.png"/>" alt="Calendar Icon"><p>${advertisement.date}</p>
                </div>
                <div class="contact">
                    <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="Contact Icon">
                    <ul>
                        <li>${advertisement.stjUserDto.name}</li>
                        <li>${advertisement.stjUserDto.email}</li>
                    </ul>
                </div>
                <a class="check-adv-link" href="profile?query=${advertisement.stjUserDto.id}">Check Advertiser Profile</a>
                <div class="description">
                    <p>${advertisement.description}</p>
                </div>
                <div class="delete-btn">
                    <form action="reports?query=deleteAdvert" method="POST">
                        <input type="hidden" name="advertisementId" value="${advertisement.id}">
                        <button type="submit" name="deleteAdvert" value="delete"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                    </form>
                </div>
            </div>
        </c:if>


        <!--TÁBLÁZAT -->
        <div class="reports-container">
            <table style="width:100%">            
                <div class="header">
                    <h2>Reports</h2>
                </div>
                <tr>
                    <th class="reporter-clmn">Reporter</th>
                    <th class="description-clmn">Description</th>
                    <th class="status-clmn">Status</th>
                </tr>
                <c:forEach items="${reportList}" var="report">
                    <tr>
                        <td class="reporter-clmn">${report.stjUserDto.name}</td>
                        <td class="description-clmn">${report.description}</td>
                        <td class="status-clmn">${report.status}</td>
                    <form action="reports" method="POST">
                        <td class="check-adv-clmn">
                            <div class="review-adv-btn"
                                 <input type="hidden" name="reportId" value="${report.id}">
                                <button name="reportButton" value="showAdvert"><img src="<c:url value="resources/images/icons/review_icon.png"/>" alt="review_icon"></button>
                        </td>
                        <td class="delete-clmn">
                            <div class="delete-report-btn">
                                <input type="hidden" name="reportId" value="${report.id}">
                                <button type="submit" name="reportButton" value="deleteReport"><img src="<c:url value="resources/images/icons/delete_icon.png"/>" alt="delete_icon"></button>
                            </div>
                        </td>
                    </form>
                    </tr>               
                </c:forEach>
            </table>
        </div>
    </body>
</html>
