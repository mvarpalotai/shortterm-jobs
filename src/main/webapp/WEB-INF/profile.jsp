
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Profile</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/profile.css"/>" />
    <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/profile_query.css"/>" />
    <body class="profile-body">
        <jsp:include page="navbar.jsp"></jsp:include>
        <c:if test="${editable == true}">
            <div class="edit-profile">
                <a href="editprofile"><img src="<c:url value="resources/images/icons/edit_icon.png"/>" alt="edit_icon"></a>
            </div>
        </c:if>
        <div class="profile-column1">
            <div class="prof-img-data-container">
                <div class="profile-picture"> 
                    <c:if test="${not empty data.profImgPath}">
                        <img class="prof-img" src="displayImage?name=${data.profImgPath}" alt="profile_picture">  
                    </c:if>
                    <c:if test="${empty data.profImgPath}">
                        <img class="prof-img" src="<c:url value="resources/images/login_avatar.jpg"/>" alt="profile_picture">
                    </c:if>
                </div>
                <div id="personal_data" class="personal-data">
                    <div class="pd-header">
                        <h3>Personal Data</h3>
                    </div>
                    <p class="user-name">${data.cvName}</p>
                    <div class="prof-home-address">
                        <img src="<c:url value="resources/images/icons/house_icon.png"/>" alt="house_icon"><p>${data.address}</p>
                    </div>
                    <div class="prof-email">
                        <img src="<c:url value="resources/images/icons/email_icon.png"/>" alt="email_icon"><p>${data.cvEmail}</p>
                    </div>
                    <div class="prof-phone">
                        <img src="<c:url value="resources/images/icons/phone_icon.png"/>" alt="phone_icon"><p>${data.phoneNumber}</p>
                    </div>
                </div>
            </div>
            <div class="work-experience">
                <div class="workexp-header"><img src="<c:url value="resources/images/icons/briefcase_icon.png"/>" alt="briefcase_icon"><h3>Work Experience</h3></div>
                    <c:forEach items="${experiences}" var="item">
                    <div class="work-exp">
                        <p class="job-position">${item.position}</p>
                        <p class="term">${item.term}</p>
                        <p class="company-name">${item.company}</p>
                        <div class="description">
                            <p>${item.description}</p>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="education">
                <div class="ed-header"><img src="<c:url value="resources/images/icons/education_icon.png"/>" alt="education_icon"><h3>Education</h3></div>
                    <c:forEach items="${educations}" var="item">
                    <div class="educ-unit">
                        <p class="school-name">${item.school}</p>
                        <p class="date">from ${item.startDate} until ${item.endDate}</p>
                        <p class="qualification">${item.qualification}</p>
                    </div>
                </c:forEach>
            </div>
        </div>


        <div class="profile-column2">
            <div class="skills">
                <div class="sk-header"><img src="<c:url value="resources/images/icons/skill_icon.png"/>" alt="skill_icon"><h3>Skills</h3></div>
                    <c:forEach items="${hard}" var="item">
                    <div class="skill">
                        <p class="skill-name">${item.name}</p>
                        <p>Level: <span class="skill-level">${item.level}</span>%</p>
                        <div class="skill-lvl-bar"><h5 class="current-skill-level"></h5></div>
                    </div>
                </c:forEach>

                <c:forEach items="${soft}" var="item">
                    <div class="skill">
                        <p class="skill-name">${item.name}</p>
                        <p class="skill-lvl">Level: <span class="skill-level">${item.level}</span>%</p>
                        <div class="skill-lvl-bar"><h5 class="current-skill-level"></h5></div>
                    </div>
                </c:forEach>
            </div>
            <div class="prof-lang">
                <div class="lang-header"><img src="<c:url value="resources/images/icons/language_icon.png"/>" alt="language_icon"><h3>Languages</h3></div>
                    <c:forEach items="${languages}" var="item">
                    <div class="lang-unit">
                        <p class="lang-name">${item.name}</p>
                        <p class="lang-lvl">Level: <span class="lang-level">${item.level}</span></p>
                        <div class="lang-lvl-bar"><h5 class="current-lang-level"></h5></div>
                    </div>
                </c:forEach>
            </div>
        </div>
        <script type="text/javascript" src="<c:url value="resources/js/profile.js"/>"></script>
    </body>
</html>

