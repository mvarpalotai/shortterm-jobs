
hideBackWardIconBtns();

function hideBackWardIconBtns() {
    var backwardIconBtns = document.getElementsByClassName("backward-icon-btn");
    var forwardIconBtns = document.getElementsByClassName("forward-icon-btn");
    for (var item in backwardIconBtns) {
        item.style.display = "none";
    } 
    for (var item in forwardIconBtns) {
        item.style.display = "none";
    } 
}


function collapseList(advertisementId) {
    var collapseBtn = document.getElementById("backward-icon-btn"+advertisementId);
    var expandBtn = document.getElementById("forward-icon-btn"+advertisementId);
    var applicantsList = document.getElementById("applicants-list"+advertisementId);

    applicantsList.style.display = "none";
    expandBtn.style.display = "inline-block";
    collapseBtn.style.display = "none";
}

function expandList(advertisementId) {
    var collapseBtn = document.getElementById("backward-icon-btn"+advertisementId);
    var expandBtn = document.getElementById("forward-icon-btn"+advertisementId);
    var applicantsList = document.getElementById("applicants-list"+advertisementId);

    applicantsList.style.display = "inline-block";
    expandBtn.style.display = "none";
    collapseBtn.style.display = "inline-block";
}


