
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Profile</title>
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/employerProfile.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/editcompanyprofile.css"/>" />
    </head>
    <body class="company-profile-body">
        <jsp:include page="navbar.jsp"></jsp:include>
            <form action="editcompanyprofile" method="POST">
                <div class="profile-column1">
                    <div id="company-data" class="company-data">
                        <div class="pd-header">
                            <h3>Company Data</h3>
                        </div>

                        <label for="name"><b>Name</b></label>
                        <input type="text" name="name" value="${company.name}" required="true">

                    <label for="cvEmail"><b>Email</b></label>
                    <input type="text" name="email" value="${company.email}" required="true">

                    <label for="address"><b>Address</b></label>
                    <input type="text" name="address" value="${company.address}" required="true">

                    <label for="size"><b>Size</b></label>
                    <input type="text" name="size" value="${company.size}" required="true">

                    <label for="hrContact"><b>HR Contact</b></label>
                    <input type="text" name="hrContact" value="${company.hrContact}">
                </div>
            </div>


            <div class="profile-column2">
                <div class="business-scope">
                    <div class="business-scope-header"><img src="<c:url value="resources/images/icons/briefcase_icon.png"/>" alt="briefcase_icon"><h3>Business Scope</h3></div>
                    <label for="businessScope"><b>Business Scope</b></label>
                    <input type="text" name="businessScope" value="${company.businessScope}" required="true">
                </div>
                <div class="business-description">
                    <div class="business-desc-header"><h3>Company Description</h3></div>
                    <label for="phoneNumber"><b>Description</b></label>
                    <input type="text" name="description" value="${company.description}">
                </div>
                <div class="save-btn">
                    <button type="submit"><img src="<c:url value="resources/images/icons/save_icon.png"/>" alt="save_icon"/></button>
                </div>
            </div>

        </form>
    </body>
</html>
