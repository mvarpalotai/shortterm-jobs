
<%@page import="com.bh10.project.shorttermjobs.dto.StjUserDto"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/advertisement.css"/>" />
        <link type="text/css" rel="stylesheet" href="<c:url value="resources/css/queries/advertisement_query.css"/>"/>
        <title>Advertisements</title>
    </head>
    <body class="advertisement-body">
        <jsp:include page="navbar.jsp"></jsp:include>
            <div class="advertisement-container">

                <div class="filters-container">
                    <div class="category-container">
                        <table id="categories">
                            <h3>Categories</h3>
                            <tr id="cat-table-rows">
                            <c:forEach items="${categories}" var="item">
                                <td><a href="advertisements?query=${item}">${item}</a></td>
                                </c:forEach>
                        </tr>
                    </table>
                    <div id="cat-collapse-btn" onclick="collapseCategory()">
                        <img src="<c:url value="resources/images/icons/slide_up_icon.png"/>" alt="Slide Down Icon">
                    </div>
                    <div id="cat-expand-btn" onclick="expandCategory()">
                        <img src="<c:url value="resources/images/icons/slide_down_icon.png"/>" alt="Slide Down Icon">
                    </div>
                </div>
                <div class="location-container">
                    <table id="locations">
                        <h3>Locations</h3>
                        <tr id="loc-table-rows">
                            <c:forEach items="${locations}" var="item">
                                <td><a href="advertisements?query=${item}">${item}</a></td>
                                </c:forEach>
                        </tr>  
                    </table>
                    <div id="loc-collapse-btn" onclick="collapseLocation()">
                        <img src="<c:url value="resources/images/icons/slide_up_icon.png"/>" alt="Slide Down Icon">
                    </div>
                    <div id="loc-expand-btn" onclick="expandLocation()">
                        <img src="<c:url value="resources/images/icons/slide_down_icon.png"/>" alt="Slide Down Icon">
                    </div>
                </div>
            </div>

            <div class="search-advert-container">
                <div class="search-container">
                    <form action="advertisements?query=search" method="post">
                        <div class="search-inputs">
                            <label for="jobType">Job Type</label>
                            <input type="text" placeholder="Enter jobtype" name="jobType">
                            <label for="location">Location</label>
                            <input type="text" placeholder="Enter location" name="location">
                        </div>
                        <button type="submit"><img class="search-icon" src="<c:url value="resources/images/icons/search_icon.png"/>" alt="Search Icon"></button>
                    </form>
                </div>

                <div class="advert-container">
                    <div class="header">
                        <h2>Advertisements</h2>
                    </div>
                    <c:if test = "${userAlreadyApplied}">
                        <h4 class="error-msg">You have already applied for this!</h4>
                    </c:if>
                    <c:if test = "${userAppliedSuccess}">
                        <h4 class="success-msg">You have successfully applied for this!</h4>
                    </c:if>
                    <c:forEach items="${advertisements}" var="item">
                        <div class="advert">
                            <div class="jobtype">
                                <img class="search-icon" src="<c:url value="resources/images/icons/job_icon.png"/>" alt="Job Icon"><p>${item.jobType}</p>
                            </div>
                            <div class="location">
                                <img class="search-icon" src="<c:url value="resources/images/icons/location_icon.png"/>" alt="Location Icon"><p>${item.location}</p>
                            </div>
                            <div class="date">
                                <img class="search-icon" src="<c:url value="resources/images/icons/calendar_icon.png"/>" alt="Calendar Icon"><p>${item.date}</p>
                            </div>
                            <div class="contact">
                                <img src="<c:url value="resources/images/icons/contact_icon.png"/>" alt="Contact Icon">
                                <ul>
                                    <li>${item.stjUserDto.name}</li>
                                    <li>${item.stjUserDto.email}</li>
                                </ul>
                            </div>
                            <a href="profile?query=${item.stjUserDto.id}">Check Advertiser Profile</a>
                            <div class="report-icon">
                                <a style="" href="reportAdvertisement?query=${item.id}"><img src="<c:url value="resources/images/icons/report_icon.png"/>" alt="report_icon"></a>
                                <p class="report-txt">Report advertisement</p>
                            </div>
                            <div class="description">
                                <p>${item.description}</p>
                            </div>
                            <c:if test="${pageContext.request.isUserInRole('user')}">
                                <div class="apply-job-cont">
                                    <form action="apply" method="post">
                                        <input type="hidden" name="advertisementId" value="${item.id}">
                                        <button type="submit" name="applySubmit">Apply</button>
                                    </form>
                                </div>
                            </c:if>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</body>
<script type="text/javascript" src="<c:url value="resources/js/advertisement.js"/>"></script>

</html>
